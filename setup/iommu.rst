IOMMU
=====

An input-output memory management unit (IOMMU) is required for safely
driving DMA-capable hardware from userspace and because of that it is
a prerequisite for using VFIO. Not all systems have one
though, so you'll need to check that the hardware supports it and
that it is enabled in the BIOS settings (``VT-d`` or ``Virtualization
Technology for Directed I/O`` on Intel systems)

Finally, IOMMU needs to be excplitly enabled in the kernel as well.
To do so, pass either ``intel_iommu=on`` (for Intel systems) or
``amd_iommu=on`` (for AMD systems) added to the kernel command line.
In addition it is recommended to use ``iommu=pt`` option which
improves IO performance for devices in the host.

Once the system boots up, check the contents of ``/sys/kernel/iommu_groups/``
directory. If it is non-empty, you have successfully set up IOMMU.

.. include:: kernelcli.inc
