Installing DPDK and related software
====================================

Once the repository setup has been done, installing DPDK is done with
YUM, as follows::

	# yum install dpdk dpdk-tools

This installs the basic dpdk runtime, the tools required to manipulate
the dpdk configuration on your system.

Where available, you might prefer ``driverctl`` over ``dpdk-tools`` 
for configuring driver bindings as the changes made with it persist
across reboots::

        # yum install driverctl

If you plan to compile or develop software utilizing the DPDK libraries,
you must install the development environment::

        # yum install dpdk-devel

This contains additional headers and library linkages.

DPDK documentation, from getting started to programmer APIs,
is also also available in packaged format for offline use::

        # yum install dpdk-doc

.. versionchanged:: Fedora 22
   In Fedora >= 22, YUM package manager has been replaced with DNF.
   To accommodate for the change, simply replace ``yum`` with ``dnf``
   in the above examples.
