Setting up DPDK
===============

.. toctree::
   :maxdepth: 3

   prerequisites
   repo
   installing
   hugepages
   iommu
   binding

