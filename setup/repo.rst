Setting up repositories
=======================

Red Hat Enterprise Linux
------------------------

DPDK and DPDK-enabled OpenvSwitch are currently available for
Red Hat Enterprise Linux 7 (x86_64 architecture only) via the
Fast Datapath Beta channel, which is not enabled by default.
To install DPDK you need to enable the channel first::

        # subscription-manager repos --enable rhel-7-fast-datapath-htb


Fedora
------

DPDK is available in the standard Fedora repositories so no additional
repository configuration is needed.
