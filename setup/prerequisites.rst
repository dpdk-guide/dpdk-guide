Important Prerequisites
=======================

Before proceeding, please ensure that the following minimum set of
requirements is met:

- Your system is x86_64 architecture
- You are running one of the following operating systems:
  - Red Hat Enterprise Linux 7.2 or newer
  - CentOS 7.2 or newer
  - Fedora 22 or newer

This document covers DPDK versions from 2.2 to 16.07.
