Hugepages
=========

2M Hugepages
------------

DPDK apps generally need hugepages support, for tyre-kicking purposes this
should be enough to get it going, Fedora and RHEL default to 2M hugepages::

	# sysctl -w vm.nr_hugepages=512

For real-world applications you'd want to make such an allocation
permanent. You can do this by adding it to ``/etc/sysctl.conf`` or a separate
file in the ``/etc/sysctl.d/`` directory.::

        # echo 'vm.nr_hugepages=512' > /etc/sysctl.d/hugepages.conf

1G Hugepages
------------

Many applications require 1GB hugepages to be used though, and the others
might perform better with them. 1GB hugepages must be allocated at boot time
via kernel command line. For example to allocate four 1GB hugepages,
you should append this to the kernel commandline when booting::

	default_hugepagesz=1GB hugepagesz=1G hugepages=4

.. include:: kernelcli.inc

Reboot to make sure it worked.
If your system is correctly configured for hugepage support, you should be able
to execute the following command and see non-zero values, corresponding to your
hugepage configuration::

        # grep HugePages_ /proc/meminfo

**NOTE** Most x86_64 systems support various sizes of hugepages. It is possible
for your system not to have 1GB support. Check by running the following::

        # if grep pdpe1gb /proc/cpuinfo >/dev/null 2>&1; then echo "1GB supported."; fi
