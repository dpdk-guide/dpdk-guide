
To permanently add this to the kernel commandline, append it to
GRUB_CMDLINE_LINUX in /etc/default/grub and then execute::

        # grub2-mkconfig -o /boot/grub2/grub.cfg

