Important cli arguments
=======================

Coremask or -list
-----------------

By default DPDK runs with all detected processor cores enabled. This is
may well suffice for simple testing or more complex applications which have
their own CPU affinity logic, but for other setups it is often desireable to
limit DPDK to specific CPUs. This can done with 
either a bitmask with the ``-c`` option or the
more human-friendly ``-l`` option to list the cores by numbers. Cores
are numbered starting from zero.

For example to enable cores 1 and 3 you could use either
``-l 1,3`` or ``-c 0xA``

Memory usage
------------

DPDK likes to hog all the hugepages it can get, which often is not
the behavior you want - for example one might want to reserve some
1G hugepages for virtual machines. Use ``--socket-mem`` (or ``-m``)
option to limit the amount of memory DPDK allocates on startup.
