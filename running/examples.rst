Examples
========

Example 1
---------

For a DPDK equivalent of "Hello world!", connect two NIC ports on the
system with a cable and run ``testpmd`` between the ports. This uses
the packet capture Poll Mode Driver so its not hardware dependent
as long as the kernel supports it, and the NIC is bound to the
kernel driver. Replace
``em1`` and ``em2`` with the appropriate interface names for your system::

    # testpmd -m256 -l1,3 \
              --vdev=eth_pcap0,iface=em1 --vdev=eth_pcap1,iface=em2 -- -i

    [...]
    Checking link statuses...
    Port 0 Link Up - speed 10000 Mbps - full-duplex
    Port 1 Link Up - speed 10000 Mbps - full-duplex
    Done
    testpmd>

    testpmd> start tx_first
      io packet forwarding - CRC stripping disabled - packets/burst=32
      nb forwarding cores=1 - nb forwarding ports=2
      RX queues=1 - RX desc=128 - RX free threshold=0
      RX threshold registers: pthresh=0 hthresh=0 wthresh=0
      TX queues=1 - TX desc=512 - TX free threshold=0
      TX threshold registers: pthresh=0 hthresh=0 wthresh=0
      TX RS bit threshold=0 - TXQ flags=0x0

    testpmd> show port stats all

      ######################## NIC statistics for port 0  ###################
      RX-packets: 2283411    RX-missed: 0          RX-bytes:  0
      RX-badcrc:  0          RX-badlen: 0          RX-errors: 0
      RX-nombuf:  0
      TX-packets: 2288633    TX-errors: 0          TX-bytes:  0
      #######################################################################

      ######################## NIC statistics for port 1  ###################
      RX-packets: 2288601    RX-missed: 0          RX-bytes:  0
      RX-badcrc:  0          RX-badlen: 0          RX-errors: 0
      RX-nombuf:  0
      TX-packets: 2283450    TX-errors: 0          TX-bytes:  0
      #######################################################################

    testpmd> stop
    Telling cores to stop...
    [...]
