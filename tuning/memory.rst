Memory
======

Memory channels
---------------

Obtaining the correct number of memory channels (the ``-n`` argument) is tricky
because it depends on how many channels the system board supports, the
number and types of memory chips and how they are physically installed in
the system and there's no easy or even reliable way to tell from a running
system.

This info should be available during bootup in the BIOS memory check stage,
during runtime ``dmidecode`` can help in making at least an educated guess.

Unless its already installed, you'll need to do so now::

   # yum install dmidecode

This usually gives sufficient amount of information to look
up the system and/or motherboard manuals to see what the board
supports and in which configurations, memory slot population is
critical to multi-channel support::

   # dmidecode -t system
   # dmidecode -t baseboard

This outputs the populated memory slots on the system::

   # dmidecode -t memory | grep 'Size: [0-9]'

If there's just one, it cannot be using multichannel memory. If there are two
or its multiple, it might be dual-channel, if there are three or its multiple
it might be triple-channel, if there are four or its multiple it might be
quad-channel. Or dual-channel...

In addition there could be further hints in the Locator fields of the memory
devices, such as ``ChannelA-DIMM0`` and ``ChannelB-DIMM0``::

   # dmidecode -t memory | grep Locator:
