Common gotchas
==============

Error messages
--------------

- ``No ethernet ports - bye``

  Ensure the devices are bound to either UIO or VFIO before starting
  the application, supported by DPDK (hardware support is limited
  compared to the kernel).

Virtual NICs
------------

Virtio-net devices are "special" in many ways:

- Leave the binding alone (ie to virtio_pci), binding to uio_pci_generic
  within the guest actually breaks it because uio_pci_generic does
  not support MSI/MSI-X interrupts.
- Regardless of binding, driver hogs all virtio-net devices by default,
  use ``-b <pci-slot>`` to blacklist any such devices dpdk should leave alone
  (eg management interface)
- The testpmd application requires ``--disable-hw-vlan`` switch when used with
  virtio-net devices, otherwise port initialization fails

.. versionchanged:: 16.04
   Starting with version 16.04, DPDK no longer takes over devices bound
   to virtio-pci driver. Instead to allow DPDK to use a virtio-net device,
   it must be unbound from all drivers.
