OVS
---

General
~~~~~~~

- Ensure the bridge was created with datapath_type=netdev
- Ensure ovs-vswitchd actually starts up, problems in DPDK setup
  typically cause it to fail to start up. With journald, you can use
  ``journalctl -t ovs-vswitchd`` to see the DPDK messages, or grep for
  ``ovs-vswitchd`` from system log files if using rsyslog or similar.

Virtual NICs
~~~~~~~~~~~~

- Ensure its NOT bound to ``uio_pci_generic``
- Ensure unique MAC addresses on each device

Port info and statistics
~~~~~~~~~~~~~~~~~~~~~~~~

- Information about port speeds and other state can be viewed with
  ``ovs-ofctl show <bridge``, for example::

    # ovs-ofctl show br0
    OFPT_FEATURES_REPLY (xid=0x2): dpid:0000ecf4bbdc0380
    n_tables:254, n_buffers:256
    capabilities: FLOW_STATS TABLE_STATS PORT_STATS QUEUE_STATS ARP_MATCH_IP
    actions: output enqueue set_vlan_vid set_vlan_pcp strip_vlan mod_dl_src mod_dl_dst mod_nw_src mod_nw_dst mod_nw_tos mod_tp_src mod_tp_dst
     1(dpdk0): addr:ec:f4:bb:dc:03:80
         config:     0
         state:      0
         current:    10GB-FD
         supported:  1GB-HD AUTO_NEG
         speed: 10000 Mbps now, 1000 Mbps max
     2(dpdk1): addr:ec:f4:bb:dc:03:82
         config:     0
         state:      0
         current:    10GB-FD
         supported:  1GB-HD AUTO_NEG
         speed: 10000 Mbps now, 1000 Mbps max
     LOCAL(br0): addr:ec:f4:bb:dc:03:80
         config:     PORT_DOWN
         state:      LINK_DOWN
         current:    10MB-FD COPPER
         speed: 10 Mbps now, 0 Mbps max
    OFPT_GET_CONFIG_REPLY (xid=0x4): frags=normal miss_send_len=0

- OVS port statistics, including transmitted and received packets, error
  counts etc can be viewed with ``ovs-ofctl dump-ports <bridge>``,
  for example::

    # ovs-ofctl dump-ports br0
    OFPST_PORT reply (xid=0x2): 3 ports
    port LOCAL: rx pkts=0, bytes=0, drop=0, errs=0, frame=0, over=0, crc=0
             tx pkts=6016480, bytes=385054720, drop=6016480, errs=0, coll=0
    port  1: rx pkts=204126460, bytes=13880599280, drop=0, errs=0, frame=?, over=?, crc=0
             tx pkts=204125884, bytes=13880557936, drop=0, errs=0, coll=?
    port  2: rx pkts=204125756, bytes=13880551408, drop=0, errs=0, frame=?, over=?, crc=0
             tx pkts=204126460, bytes=13880597104, drop=0, errs=0, coll=?

- PMD statistics (primarily useful for performance tuning) can be
  viewed with ``ovs-appctl dpif-netdev/pmd-stats-show`` command.
  For example::

    # ovs-appctl dpif-netdev/pmd-stats-show
    main thread:
            emc hits:0
            megaflow hits:0
            miss:0
            lost:0
            polling cycles:73897097 (100.00%)
            processing cycles:0 (0.00%)
    pmd thread numa_id 0 core_id 0:
            emc hits:69249886
            megaflow hits:32
            miss:2
            lost:0
            polling cycles:6539496906301 (98.69%)
            processing cycles:86741963460 (1.31%)
            avg cycles per packet: 95685.87 (6626238869761/69249920)
            avg processing cycles per packet: 1252.59 (86741963460/69249920)

- See ``man 8 ovs-vswitchd`` for numerous other datapath and interface
  debugging commands.
