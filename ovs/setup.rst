OpenvSwitch Setup
=================

Installation
------------

For Red Hat Enterprise Linux, DPDK-enabled OpenvSwitch is available
in the Fast Datapath Beta channel, to subscribe it see :doc:`../setup/repo`

Once subscribed to the channel, installation is simply::

    # yum install openvswitch

Basic setup
-----------

Enabling DPDK ports in OVS requires a bit of extra setup - like all
DPDK-applications it needs those
:doc:`mandatory arguments <../running/cli>` passed to it. This is
done through DPDK_OPTIONS environment variable in ``/etc/sysconfig/openvswitch``
file, for example::

    OPTIONS=""
    DPDK_OPTIONS="-l1,3 -n4"

After that, the OVS service can be started normally::

    # systemctl start openvswitch.service

If it fails to start properly, see :doc:`../troubleshooting/index`.
Note that with OVS, manually loading drivers with ``-d`` is not usually
needed, but there's also no harm in doing so.
