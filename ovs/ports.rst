DPDK Ports
==========

DPDK ports can only be added to a userspace bridge. To create one,
set the bridge ``datapath_type`` to ``netdev``. For example to create
bridge named ``br0`` for use with DPDK ports::

    # ovs-vsctl add-br br0 -- set bridge br0 datapath_type=netdev

Physical NIC
------------

A physical DPDK NIC is presented in OVS as a port type ``dpdk``. For
dpdk-type ports, the port name must consist of "dpdk" prefix, followed
by portid number of a detected DPDK device. For example::

    # ovs-vsctl add-port br0 dpdk0 -- set Interface dpdk0 type=dpdk
    # ovs-vsctl add-port br0 dpdk1 -- set Interface dpdk1 type=dpdk

Note that any NICs must be bound to the correct driver first,
see :doc:`../setup/binding`.

Bonding
-------

When creating bonds of DPDK interfaces, the port type must be explicitly
specified for each interface, like with any DPDK port. For example::

    # ovs-vsctl add-bond br0 bond0 dpdk0 dpdk1 -- set Interface dpdk0 type=dpdk -- set Interface dpdk1 type=dpdk

vhost-user
----------

There are no restrictions for port naming with vhost-user ports,
but since these ports are actually sockets on the file system
the path needs to be free for use. By default the socket path
for vhost-user ports is OVS runtime directory, typically
``/var/run/openvswitch``. The port type for vhost-user is ``dpdkvhostuser``.
For example::

     # ovs-vsctl add-port br0 myport1 -- set Interface myport1 type=dpdkvhostuser
