Contributing
------------

This document has been created using `Sphinx <http://sphinx-doc.org/>`_
and its source is available in a public git repository here:
https://gitlab.com/dpdk-guide/dpdk-guide

If you notice mistakes, omissions of important items or have other
ideas to improve the document, you're welcome to file issues and
merge requests on the project page.

