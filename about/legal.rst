Legal notice
------------

Copyright © 2015 Red Hat, Inc.

This document is licensed by Red Hat under the
`Creative Commons Attribution-ShareAlike 3.0 Unported License
<http://creativecommons.org/licenses/by-sa/3.0/>`_.
If you distribute this document, or a modified version of it, you must
provide attribution to Red Hat, Inc. and provide a link to the original.
