OpenvSwitch
-----------

Once the :doc:`environment` has been set up, compiling OVS from source
with DPDK support is simple::

    $ git clone https://github.com/openvswitch/ovs.git
    $ cd ovs
    $ ./configure --with-dpdk=$RTE_SDK
    $ make
