Development environment
-----------------------

Refer to :doc:`../setup/installing` for installation of packages
required for development.

Right after installation you should either log out and back in again,
or manually perform the following to pull the environment variables
into your session::

    $ unset RTE_SDK
    $ . /etc/profile.d/dpdk-sdk-`uname -m`.sh

