DPDK example applications
--------------------------

Once the :doc:`environment` has been set up, compiling DPDK example 
applications is simple. For example this
fetches the upstream sources, checks out the appropriate version
and finally compiles and executes the L3-forwarding example application::

    $ git clone git://dpdk.org/dpdk
    $ cd dpdk
    $ git checkout v`rpm -q --qf %{version} dpdk`
    $ make
    $ build/l3fwd --help

